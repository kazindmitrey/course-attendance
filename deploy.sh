#!/usr/bin/bash

# defining environment to deploy
STAGE=$1
PROD=172.31.43.158
STAGING=172.31.43.103
DEPLOY_ENV=''

if [[ $STAGE == production ]]
then
	DEPLOY_ENV=$PROD
elif [[ $STAGE == staging ]]
then
	DEPLOY_ENV=$STAGING
else
	exit 1
fi

echo "DEPLOY TO ${STAGE} ENVIRONMENT: START"

# copying docker-compose-prod.yaml to production server
scp -i "~/.ssh/id_rsa" ./docker-compose-prod.yml \
ec2-user@$DEPLOY_ENV:/home/ec2-user/

# copying mysql-env to production server
scp -i "~/.ssh/id_rsa" -r ./env/ \
ec2-user@$DEPLOY_ENV:/home/ec2-user/

# login with ssh to ec2-user & bring the application up
ssh -i "~/.ssh/id_rsa" \
ec2-user@$DEPLOY_ENV \
-o BatchMode=yes -o StrictHostKeyChecking=no \
<< EOF
cd /home/ec2-user/
docker-compose -f docker-compose-prod.yml down
docker images -q | xargs docker rmi -f
docker-compose -f docker-compose-prod.yml up -d --build
EOF

